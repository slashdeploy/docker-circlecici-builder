@test "aws CLI installed" {
	run aws --version
	[ $status -eq 0 ]
}

@test "gcloud SDK installed" {
	run gcloud --version
	[ $status -eq 0 ]
}

@test "docker installed" {
	run docker --version
	[ $status -eq 0 ]
}

@test "docker-compose installed" {
	run docker-compose --version
	[ $status -eq 0 ]
}

@test "kubectl installed" {
	run kubectl version --client
	[ $status -eq 0 ]
}

@test "git installed" {
	run git --version
	[ $status -eq 0 ]
}

@test "ssh client installed" {
	run ssh -V
	[ $status -eq 0 ]
}

@test "yq installed" {
	run yq --version
	[ $status -eq 0 ]
}

@test "jq installed" {
	run jq --version
	[ $status -eq 0 ]
}

@test "make installed" {
	run make --version
	[ $status -eq 0 ]
}

@test "shellcheck installed" {
	run shellcheck --version
	[ $status -eq 0 ]
}

@test "runs as CircleCI user" {
	run whoami
	[ "${output}" = "circleci" ]
}

@test "supports nopassword sudo" {
	run sudo echo hello
	[ "${output}" = "hello" ]
}
