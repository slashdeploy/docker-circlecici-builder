FROM alpine:3.10

RUN apk add gcc python-dev musl-dev libffi-dev openssl-dev

RUN apk add \
	bats \
	curl \
	docker-cli \
	git \
	jq \
	make \
	py-pip \
	python \
	openssh-client \
	shadow \
	sudo

RUN pip install awscli docker-compose 'pyYAML<4.3' 'urllib3<1.25'

RUN curl -sSL https://storage.googleapis.com/shellcheck/shellcheck-stable.linux.x86_64.tar.xz > /tmp/shellcheck.tar.xz \
	&& tar Jxf /tmp/shellcheck.tar.xz -C /tmp \
	&& chmod +x /tmp/shellcheck-*/shellcheck \
	&& mv /tmp/shellcheck-*/shellcheck /usr/bin \
	&& rm -rf /tmp/shellcheck.tar.xz /tmp/shellcheck-*

RUN curl -sSL -o /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64 \
	&& chmod +x /usr/local/bin/yq

RUN curl -sSL https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-260.0.0-linux-x86_64.tar.gz > /tmp/gcloud.tar.gz \
 && mkdir -p /opt/google-cloud-sdk \
 && tar zxf /tmp/gcloud.tar.gz -C /opt/google-cloud-sdk --strip-components 1 \
 && /opt/google-cloud-sdk/install.sh --additional-components kubectl

ENV PATH /opt/google-cloud-sdk/bin:$PATH

COPY self_test.bats /usr/share/self_test.bats

RUN groupadd --gid 3434 circleci \
  && useradd --uid 3434 --gid circleci --shell /bin/bash --create-home circleci \
  && echo 'circleci ALL=NOPASSWD: ALL' >> /etc/sudoers.d/50-circleci

USER circleci

CMD [ "/bin/sh" ]
